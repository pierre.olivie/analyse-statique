//int where_are_the_run_time_errors(int input) {
float where_are_the_run_time_errors(int input) {
	float x,y,k;
	
	k = input / 100; 
	x = 2;
	y = k + 5; 
	
	while (x < 10) {
		x++;
		y = y + 3;
	}
	
	if ((3*k + 100) > 43) {
		y++;
		if ((x>0 && x<15) && (y>5 && y<35)) {
			x = (10 * x) / (x - y);
		}
	}
	return x; //
}

//--- Exercice 2 ---
/* Q2.a) apres lecture du programme, on s'appercois que une possible division par 0 ssi y=10 et x=10. Pour cela, k=-20. Or si k=-20, la condition if n'est pas v�rifi�e.
 *
 *Q2.b) apres Code prover, polyspace ne detecte pas d'erreur
 *
 *Q2.c) Si le Code Prover utilisait les intervalles abstraits, alors les variables x & y pourrait etre = en prenant une valeur entre 6 et 15, ce qui impliquerait une possible division par 0. Le resultat montre que le CP de Polyspace utilise une technique d'analyse.
 *
 *Q2.d)Lorsque l'on redeclare la foncton avec des floats, code prover detecte une possible erreur sur la division par 0. Pour �viter cela, on peut ajouter une condition dans le programme permettant de verifier que x != 0 avant d'�ffectuer le calcul */