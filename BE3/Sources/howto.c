extern int read_an_input(void);

static exo1_red(void)
{
  int X;

  X = 1 / X; //x n'a pas �t� initialis� - Red Check
  X = X + 1;
}

static exo2_grey(void)
{
  int X, Z;
     
  X = read_an_input(); //functions read_an_input returns a initialized value
  if (X > 5 && X < 10) { //x n'a pas valeuR pour entrer dans cette condition. code inutile - grey check
    if (X < 5) { // x is set before being used but not declare
      X = X + 1; // x is set before being used but not declare
      Z = 1 / X;
      X += Z - 1;
    }
  }   
}

static exo3_propagate(void)
{
  int X;
  int Y[100];

  X = read_an_input(); //functions read_an_input returns a initialized value
  Y[X] = 0; //X n'a pas �t� initialis� - Indice inconnu pour le tableau
  Y[X] = 0; //tableau indice inconnu - green check
}

static exo4_explore(void)
{
  int X;

  if (read_an_input())  //functions read_an_input returns a initialized value
    X = 100;
	       //Ici, l'initialisation de x n'est pas faites correctement, X non initialis� quand appel� - Orange check
  if (X > 101) //x n'a pas la valeur suffisante pour entrer dans cette condition. code inutile, donc grey check
    X = X + 1;
}

static exo5_linear(void)
{
  int X;
  int Y[100];

  X = read_an_input(); //functions read_an_input returns a initialized value green check
  Y[X] = 0; // Indice inconnu pour le tableau - green check
  Y[X-1] = (1 / X) + X ; //division par 0 - orange check and the operations don't overflow the INT32 - green check
  if (X == 0)  //x n'a pas la valeur pour entrer dans cette condition. code inutile, donc grey check
    Y[X] = 1;
}

int main(void) 
{

  if (read_an_input()) exo1_red();//functions read_an_input returns a initialized value
  exo2_grey();
  exo3_propagate();
  exo4_explore();
  exo5_linear();

}

